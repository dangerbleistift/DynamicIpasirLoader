This is a dynamic loader prototype for sat-solvers, that use the ipasir API (see https://github.com/biotomas/ipasir)

The Dynamic loader can be used with dynamic libraries, so it is necessary to compile given sat solvers not as as static, but as dynamic libraries.

