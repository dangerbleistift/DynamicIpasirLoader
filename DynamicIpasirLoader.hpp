#pragma once

#include <string>
#include <map>
#include <dlfcn.h>

namespace SolverProxy
{
namespace Sat
{

class DynamicIpasirLoader
{
	public:
		DynamicIpasirLoader();
		virtual ~DynamicIpasirLoader();

		void LoadLibrary(std::string);
		void CloseCurrentLibrary();
		bool Status();

		// Ipasir functions

		void IpasirAdd(void * solver, int32_t lit_or_zero) const;
		void IpasirAssume(void * solver, int32_t lit) const;
		int IpasirFailed (void * solver, int32_t lit) const;
		void * IpasirInit() const;
		void IpasirSetLearn (void * solver, void * data, int max_length, void (*learn)(void * data, int32_t * clause)) const;
		void IpasirSetTerminate (void * solver, void * data, int (*terminate)(void * data)) const;
		const char * IpasirSignature() const;
		int IpasirSolve(void * solver) const;
		void IpasirRelease(void * solver) const;
		int32_t IpasirVal(void * solver, int32_t lit) const;

	private:	
		void LogError();
		void LoadSymbols();
		void* LoadSymbol(std::string);

		bool _status = false;
		std::map<std::string, void*> _functionHandles;
		void * _libraryHandle;
};

}

}
