#include <iostream>

#include "DynamicIpasirLoader.hpp"

using namespace SolverProxy::Sat;

int main()
{

    std::cout << "Starting main()\n\n";
    DynamicIpasirLoader loader = DynamicIpasirLoader();

    loader.LoadLibrary("./libipasirpicosat961.so");
    // Test multiple Loadings.
    loader.LoadLibrary("./libipasirpicosat961.so");

    if ( ! loader.Status() )
    {
        std::cout << "Error during dynamic loading process\n";
        return -1;
    }

    auto solver = loader.IpasirInit();
    std::cout << "Currently loaded: " << loader.IpasirSignature() << "\n";

}