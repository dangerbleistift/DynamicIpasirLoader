#include "DynamicIpasirLoader.hpp"

#include <dlfcn.h>
#include <iostream>


namespace SolverProxy
{
namespace Sat
{

// Creates a new empty dynamic loader instance
DynamicIpasirLoader::DynamicIpasirLoader() {
	std::cout << "New Loader created \n";
	_libraryHandle = nullptr;
}

//  Closes the currently open library, if it exits
DynamicIpasirLoader::~DynamicIpasirLoader() {
	CloseCurrentLibrary();
	std::cout << "\n\n";
}

// Tries to open given dynamic library.
// Logs error if loading failed.
void DynamicIpasirLoader::LoadLibrary(std::string libraryPath)
{	
	// Make sure, that recent opened library is unloaded properly.
	if (_libraryHandle != 0)
	{
		CloseCurrentLibrary();
	}

	_libraryHandle = dlopen(libraryPath.c_str(), RTLD_NOW);

	if (_libraryHandle == 0)
	{
		LogError();
		return;
	}

	LoadSymbols();
}

// Closes the currently loaded dynamic library via its handle (if existing)
void DynamicIpasirLoader::CloseCurrentLibrary()
{
	if (_libraryHandle == 0)
	{
		std::cout << "No library opened currently\n";
		return;
	}
	std::cout << "Closing Current Library\n";
	if (dlclose(_libraryHandle) != 0)
	{
		LogError();
	}
	_status = false;
	_libraryHandle = nullptr;
}

// Returns status of dynamic loader.
// Is true, if library could be opened and symbols are loaded correctly.
bool DynamicIpasirLoader::Status()
{
	return _status;
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void DynamicIpasirLoader::IpasirAdd(void * solver, int32_t lit_or_zero) const
{
	reinterpret_cast<void (*)(void*, int32_t)> (_functionHandles.at("ipasir_add"))(solver, lit_or_zero);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void DynamicIpasirLoader::IpasirAssume(void * solver, int32_t lit) const
{
	reinterpret_cast<void (*)(void*, int32_t)> (_functionHandles.at("ipasir_assume"))(solver, lit);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void* DynamicIpasirLoader::IpasirInit() const
{
	return reinterpret_cast<void* (*)()> (_functionHandles.at("ipasir_init"))();
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void DynamicIpasirLoader::IpasirRelease(void * solver) const
{
	reinterpret_cast<void (*)(void*)> (_functionHandles.at("ipasir_release"))(solver);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
const char * DynamicIpasirLoader::IpasirSignature() const
{
	return reinterpret_cast<const char * (*)()> (_functionHandles.at("ipasir_signature"))();
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void DynamicIpasirLoader::IpasirSetLearn(void * solver, void * data, int max_length, void (*learn)(void * data, int32_t * clause)) const
{
	reinterpret_cast<void (*)(void*, void*, int, void (*learn)(void * data, int32_t * clause))> (_functionHandles.at("ipasir_set_learn"))(solver, data, max_length, learn);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
void DynamicIpasirLoader::IpasirSetTerminate(void * solver, void * data, int (*terminate)(void * data)) const
{
	reinterpret_cast<void (*)(void*, void*, int (*terminate)(void * data))> (_functionHandles.at("ipasir_set_terminate"))(solver, data, terminate);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
int DynamicIpasirLoader::IpasirSolve(void * solver) const
{
	return reinterpret_cast<int (*)(void*)> (_functionHandles.at("ipasir_solve"))(solver);
}

// See documentation at https://github.com/biotomas/ipasir/blob/master/ipasir.h
int32_t DynamicIpasirLoader::IpasirVal(void * solver, int32_t lit) const
{
	return reinterpret_cast<int32_t (*)(void*, int32_t)> (_functionHandles.at("ipasir_val"))(solver, lit);
}

// Log the newest occured error.
void DynamicIpasirLoader::LogError()
{
	std::cerr << dlerror()<< "\n";;
}

// Loads all ipasir functions from shared library.
// Function handles are stored in map.
void DynamicIpasirLoader::LoadSymbols()
{
	std::cout << "Loading Symbols\n";

	// If any of LoadSymbols() fails, _status will be false.
	_status = true;

	_functionHandles["ipasir_add"] = LoadSymbol("ipasir_add");
	_functionHandles["ipasir_assume"] = LoadSymbol("ipasir_assume");
	_functionHandles["ipasir_failed"] = LoadSymbol("ipasir_failed");
	_functionHandles["ipasir_init"] = LoadSymbol("ipasir_init");
	_functionHandles["ipasir_release"] = LoadSymbol("ipasir_release");
	_functionHandles["ipasir_set_learn"] = LoadSymbol("ipasir_set_learn");
	_functionHandles["ipasir_set_terminate"] = LoadSymbol("ipasir_set_terminate");
	_functionHandles["ipasir_signature"] = LoadSymbol("ipasir_signature");
	_functionHandles["ipasir_solve"] = LoadSymbol("ipasir_solve");
	_functionHandles["ipasir_val"] = LoadSymbol("ipasir_val");
}

// Tries to load given symbol name from the currently loaded library.
void* DynamicIpasirLoader::LoadSymbol(std::string symbolName)
{
	std::cout << "Loading symbol: " << symbolName << "\n";
	void *functionHandle = dlsym(_libraryHandle, symbolName.c_str());

	if (functionHandle == 0)
	{
		LogError();
		_status = false;
	}

	return functionHandle;
}

}

}
